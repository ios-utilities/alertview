//
//  ViewController.swift
//  AlertView
//
//  Created by SD on 06/10/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func promptBottomAlert(_ title: String = "")  {
        debugPrint("API call in repeat")
        self.view.showBottomAlert(with: "This is Bottom Alert!",title: title, type: .error,hideRetryButton: false) {[weak self] in
            guard let self = self else { return }
            //debugPrint("Retry button pressed in completion")
            
            self.promptBottomAlert()
        }
        
        
    }

    @IBAction func actionTopAlert(_ sender: UIButton) {
        //NetworkMonitor.shared.stop()
        self.view.showTopAlert(with: "This is Top alert!",type: .warning)
    }
    
    @IBAction func actionBottomAlert(_ sender: UIButton) {
        //NetworkMonitor.shared.start()
        self.promptBottomAlert("With title!")
    }
    
    @IBAction func actionAlertFromAnyView(_ sender: UIButton) {
        self.topMostController()?.view.showBottomAlert(with: "This is info alert and can be shown from any view like UITableviewCell / UICollectionviewCell / Subviews", type: .info)
    }
}

