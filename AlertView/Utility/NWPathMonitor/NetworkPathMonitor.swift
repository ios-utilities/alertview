//
//  NetworkPathMonitor.swift
//  BoxBrownie
//
//  Created by SD on 07/10/20.
//  Copyright © 2020  All rights reserved.
//

import Network
import UIKit

final class NetworkMonitor {
    static let shared = NetworkMonitor()
    private init(shouldInitialize: Bool = true) {
        if shouldInitialize && monitor == nil {
            monitor = NWPathMonitor()
            self.commonInit()
        }
    }
    private var queue = DispatchQueue(label: "monitorNetwork")
    private var isSuspendedQueue = false
    private var monitor: NWPathMonitor? = nil//NWPathMonitor(requiredInterfaceType: .cellular)
    private var window: UIWindow? {
        guard let scene = UIApplication.shared.connectedScenes.first,
              let windowSceneDelegate = scene.delegate as? UIWindowSceneDelegate,
              let window = windowSceneDelegate.window else {
            return nil
        }
        return window
    }
    convenience init(requiredInterfaceType: NWInterface.InterfaceType) {
        self.init(shouldInitialize: false)

        monitor = NWPathMonitor(requiredInterfaceType: requiredInterfaceType)
        commonInit()
    }
    
    @available(iOS 14.0, *)
    convenience init(excludeInterfaceTypes: [NWInterface.InterfaceType]) {
        self.init(shouldInitialize: false)
        
        monitor = NWPathMonitor(prohibitedInterfaceTypes: excludeInterfaceTypes)
        commonInit()
    }
    
    var currentPath: NWPath? {
        return self.monitor?.currentPath
    }

    fileprivate func commonInit() {
        self.monitor?.pathUpdateHandler = {[weak self] path in
            guard let self = self else {
                return
            }
            print("Newtwork status:",self.monitor?.currentPath as Any)
            switch path.status {
            case .satisfied:
                DispatchQueue.main.async {
                    self.window?.rootViewController?.view.showTopAlert(with: "Internet connection back online!", type: .success)
                    //For appdelegate guys (who don't use scene delegate), below code
                    //UIWindow.key?.rootViewController?.view.showTopAlert(with: "Internet connection back online!", type: .success)
                }
                break
            case .unsatisfied:
                DispatchQueue.main.async {
                    self.window?.rootViewController?.view.showTopAlert(with: "Internet connection gone offline!", type: .error)
                    //For appdelegate guys (who don't use scene delegate), below code
                    //UIWindow.key?.rootViewController?.view.showTopAlert(with: "Internet connection gone offline!", type: .error)
                }
                break
            case .requiresConnection:
                DispatchQueue.main.async {
                    self.window?.rootViewController?.view.showTopAlert(with: "No stable internet connection found!", type: .error)
                    //For appdelegate guys (who don't use scene delegate), below code
                    //UIWindow.key?.rootViewController?.view.showTopAlert(with: "No stable internet connection found!", type: .error)
                }
                break
            default:
                break
            }
        }
        
        
        self.start()
    }
    
    func start()  {
        if self.isSuspendedQueue {
            queue.resume()
            self.isSuspendedQueue = false
        } else {
            self.monitor?.start(queue: queue)
        }
    }
    
    func stop()  {
        if !self.isSuspendedQueue {
            queue.suspend()
            self.isSuspendedQueue = true
        }
    }
}

