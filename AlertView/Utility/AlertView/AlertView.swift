//
//  AlertView.swift
//  BoxBrownie
//
//  Created by SD on 05/10/20.
//  Copyright © 2020 UIPL. All rights reserved.
//

import UIKit

enum AlertDirection {
    case top
    case bottom
}

enum AlertType {
    case success
    case error
    case info
    case warning
}
class AlertView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var btnTopClose: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblTopMessage: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var completionRetryAction: (() -> Void)? = {}
    var alertType: AlertType = .success
    var direction: AlertDirection = .bottom
    var titleText = ""
    var messageText = ""
    var retryTitleText = ""
    override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
        }

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
        }

    
    /// Common method for Alert view setup
    /// - Parameters:
    ///   - title: String value for title if any
    ///   - message: String value for message
    ///   - type: AlertType enum value for  type like
    ///      - success
    ///      - error
    ///      - info
    ///      - warning.
    ///   - direction: AlertDirection enum value direction like
    ///      - Top
    ///      - Bottom
    ///   - retryTitle: String value for retry buttion title
    ///   - shouldHideRetry: Boolean value to hide retry button
    func commonInit(_ title: String = "",message: String = "",type: AlertType = .success,direction: AlertDirection = .bottom,retryTitle: String = "",shouldHideRetry: Bool = false) {
        self.backgroundColor = .clear
        self.titleText = title
        self.messageText = message
        
        guard let _ = self.bottomView,let _ = self.lblTitle,let _ = self.lblMessage,let _ = self.btnRetry,let _ = self.topView,let _ = self.lblTopMessage else {
            return
        }
        self.setupAlertUI(type,direction,retryTitle,shouldHideRetry)
    }

    
    /// Top and Bottom view accesories UI and value setup
    /// - Parameters:
    ///   - alertType: AlertType enum value for  type like
    ///      - success
    ///      - error
    ///      - info
    ///      - warning.
    ///   - direction: AlertDirection enum value direction like
    ///      - Top
    ///      - Bottom
    ///   - retryTitle: String value for retry buttion title
    ///   - shouldHideRetry: Boolean value to hide retry button
    func setupAlertUI(_ alertType: AlertType,_ direction: AlertDirection,_ retryTitle: String = "", _ shouldHideRetry: Bool = false) {
        self.alertType = alertType
        self.direction = direction
        self.retryTitleText = retryTitle
        
        switch self.alertType {
        case .error:
            switch direction {
            case .top:
                self.lblTopMessage.text = self.messageText
                self.btnTopClose.setTitle(self.retryTitleText.isEmpty ? "OK" : retryTitle.uppercased(), for: .normal)
                self.topView.alpha = 1
                self.topView.backgroundColor = UIColor(hex: "ffeff0")
                self.lblTopMessage.textColor = UIColor(hex: "f83839")
                self.lblTopMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnTopClose.titleLabel?.font = UIFont(.semibold, inSize: 15) ?? .systemFont(ofSize: 15)
                self.btnTopClose.setTitleColor(UIColor(hex: "f83839"), for: .normal)
                self.btnTopClose.isHidden = shouldHideRetry
                self.lblTopMessage.textAlignment = shouldHideRetry ? .center : .left
                break
            case .bottom:
                self.lblTitle.text = self.titleText
                self.lblMessage.text = self.messageText
                self.bottomView.alpha = 1
                self.btnRetry.setTitle(self.retryTitleText.isEmpty ? "RETRY" : self.retryTitleText.uppercased(), for: .normal)
                self.bottomView.backgroundColor = UIColor(hex: "f8383915")
                self.lblTitle.textColor = UIColor(hex: "404040")
                self.lblTitle.font = UIFont(.semibold, inSize: 14) ?? .systemFont(ofSize: 14)
                self.lblMessage.textColor = UIColor(hex: "404040")
                self.lblMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnRetry.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnRetry.titleLabel?.textColor = UIColor(hex: "f83839")
                self.btnCancel.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnCancel.titleLabel?.textColor = UIColor(hex: "f83839")
                self.lblTitle.isHidden = self.titleText.isEmpty
                self.btnRetry.isHidden = shouldHideRetry
                self.btnCancel.isHidden = shouldHideRetry
                break
            }
        case .info:
            switch direction {
            case .top:
                self.lblTopMessage.text = self.messageText
                self.btnTopClose.setTitle(self.retryTitleText.isEmpty ? "OK" : retryTitle.uppercased(), for: .normal)
                self.topView.alpha = 1
                self.topView.backgroundColor = UIColor(hex: "f2f4ff")
                self.lblTopMessage.textColor = UIColor(hex: "5676ff")
                self.lblTopMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnTopClose.titleLabel?.font = UIFont(.semibold, inSize: 15) ?? .systemFont(ofSize: 15)
                self.btnTopClose.setTitleColor(UIColor(hex: "5676ff"), for: .normal)
                self.btnTopClose.isHidden = shouldHideRetry
                self.lblTopMessage.textAlignment = shouldHideRetry ? .center : .left
                break
            case .bottom:
                self.lblTitle.text = self.titleText
                self.lblMessage.text = self.messageText
                self.bottomView.alpha = 1
                self.btnRetry.setTitle(self.retryTitleText.isEmpty ? "RETRY" : self.retryTitleText.uppercased(), for: .normal)
                self.bottomView.backgroundColor = UIColor(hex: "f2f4ff")
                self.lblTitle.textColor = UIColor(hex: "5676ff")//404040
                self.lblTitle.font = UIFont(.semibold, inSize: 14) ?? .systemFont(ofSize: 14)
                self.lblMessage.textColor = UIColor(hex: "5676ff")//404040
                self.lblMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnRetry.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnRetry.titleLabel?.textColor = UIColor(hex: "f83839")
                self.btnCancel.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnCancel.titleLabel?.textColor = UIColor(hex: "f83839")
                self.lblTitle.isHidden = self.titleText.isEmpty
                self.btnRetry.isHidden = shouldHideRetry
                self.btnCancel.isHidden = shouldHideRetry
                break
            }
        case .warning:
            switch direction {
            case .top:
                self.lblTopMessage.text = self.messageText
                self.btnTopClose.setTitle(self.retryTitleText.isEmpty ? "OK" : retryTitle.uppercased(), for: .normal)
                self.topView.alpha = 1
                self.topView.backgroundColor = UIColor(hex: "fff7eb")
                self.lblTopMessage.textColor = UIColor(hex: "ff9200")
                self.lblTopMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnTopClose.titleLabel?.font = UIFont(.semibold, inSize: 15) ?? .systemFont(ofSize: 13)
                self.btnTopClose.setTitleColor(UIColor(hex: "ff9200"), for: .normal)
                self.btnTopClose.isHidden = shouldHideRetry
                self.lblTopMessage.textAlignment = shouldHideRetry ? .center : .left
                break
            case .bottom:
                self.lblTitle.text = self.titleText
                self.lblMessage.text = self.messageText
                self.bottomView.alpha = 1
                self.btnRetry.setTitle(self.retryTitleText.isEmpty ? "RETRY" : self.retryTitleText.uppercased(), for: .normal)
                self.bottomView.backgroundColor = UIColor(hex: "fff7eb")
                self.lblTitle.textColor = UIColor(hex: "ff9200")//404040
                self.lblTitle.font = UIFont(.semibold, inSize: 14) ?? .systemFont(ofSize: 14)
                self.lblMessage.textColor = UIColor(hex: "ff9200")//404040
                self.lblMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnRetry.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnRetry.titleLabel?.textColor = UIColor(hex: "f83839")
                self.btnCancel.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnCancel.titleLabel?.textColor = UIColor(hex: "f83839")
                self.lblTitle.isHidden = self.titleText.isEmpty
                self.btnRetry.isHidden = shouldHideRetry
                self.btnCancel.isHidden = shouldHideRetry
                break
            }
        case .success:
            switch direction {
            case .top:
                self.lblTopMessage.text = self.messageText
                self.btnTopClose.setTitle(self.retryTitleText.isEmpty ? "OK" : retryTitle.uppercased(), for: .normal)
                self.topView.alpha = 1
                self.topView.backgroundColor = UIColor(hex: "ebf9f0")
                self.lblTopMessage.textColor = UIColor(hex: "00b441")
                self.lblTopMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnTopClose.titleLabel?.font = UIFont(.semibold, inSize: 15) ?? .systemFont(ofSize: 15)
                self.btnTopClose.setTitleColor(UIColor(hex: "00b441"), for: .normal)
                self.btnTopClose.isHidden = shouldHideRetry
                self.lblTopMessage.textAlignment = shouldHideRetry ? .center : .left
                break
            case .bottom:
                self.lblTitle.text = self.titleText
                self.lblMessage.text = self.messageText
                self.bottomView.alpha = 1
                self.btnRetry.setTitle(self.retryTitleText.isEmpty ? "RETRY" : self.retryTitleText.uppercased(), for: .normal)
                self.bottomView.backgroundColor = UIColor(hex: "57dd8915")
                self.lblTitle.textColor = UIColor(hex: "404040")
                self.lblTitle.font = UIFont(.semibold, inSize: 14) ?? .systemFont(ofSize: 14)
                self.lblMessage.textColor = UIColor(hex: "404040")
                self.lblMessage.font = UIFont(.regular, inSize: 14) ?? .systemFont(ofSize: 14)
                self.btnRetry.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnRetry.titleLabel?.textColor = UIColor(hex: "f83839")
                self.btnCancel.titleLabel?.font = UIFont(.semibold, inSize: 13) ?? .systemFont(ofSize: 13)
                self.btnCancel.titleLabel?.textColor = UIColor(hex: "f83839")
                self.lblTitle.isHidden = self.titleText.isEmpty
                self.btnRetry.isHidden = shouldHideRetry
                self.btnCancel.isHidden = shouldHideRetry
                break
            }
        }
    }

    
    //MARK:- Actions
    
    /// Outside tap action for dismiss
    /// - Parameter sender: outside button
    @IBAction func actionDismissView(_ sender: UIButton) {
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.alpha = 0
        }) { _ in
                self.removeFromSuperview()
        }
    }
    
    
    /// Bottom view retry button action
    /// - Parameter sender: retry button
    @IBAction func actionRetry(_ sender: UIButton) {
        debugPrint("Retry pressed")
        self.completionRetryAction?()
        self.removeFromSuperview()
    }
    /// Bottom view cancel button action
    /// - Parameter sender: cancel button
    @IBAction func actionCancel(_ sender: UIButton) {
        debugPrint("Retry pressed")
        self.removeFromSuperview()
    }
    
    /// Top view dismiss button action
    /// - Parameter sender: dismiss button
    @IBAction func actionDismissTopView(_ sender: UIButton) {
        debugPrint("Close button pressed")
        self.removeFromSuperview()
    }
    
}
