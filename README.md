# AlertView

**The above project may not run below XCode12**,Suggestion is to copy the [**"Utility Folder"**](https://gitlab.com/ios-utilities/alertview/-/tree/master/AlertView/Utility)  to your project, & its ready to go.

Show alerts with respect to views on top / bottom position of the page irrespective of safearea.
Like:
- One can show Internet connection offline message on top of the page .
- There is retry or cancel button on the bottom alert which is optional.
- The alert can be automatically hidden.
- There is also a Network monitor integrated into this project (Internet connection Online / offline Monitor). For details go to [**Here**](https://gitlab.com/ios-utilities/swiftnetworkmonitor)
- There are basically 4 kinds of themes , like as follows:-

1. Success 
1. Error
1. Warning
1. Info

[**Here is the Attached Images**](https://drive.google.com/drive/folders/1s5tnpaRg8lmZomztJUxrRZYIdg9Qlhp9?usp=sharing)



There is also an completionblock for retry button for bottom alert.

Usage:

- [ ] A. Top alert: 
    `self.view.showTopAlert(with: "This is Top alert!",type: .warning)` 
    **One can disable the self dismiss of alert , then there will be customize "OK" button and by clicking on ok button the alert will dismiss, like:-** 
    `self.view.showTopAlert(with: "This is Top alert!",type: .warning,autoDismiss: false)`

- [ ] B. Bottom alert:
    `self.view.showBottomAlert(with: "This is Bottom Alert!",title: title, type: .error,hideRetryButton: false) {[weak self] in
            guard let self = self else { return }
            debugPrint("Retry button pressed in completion")
            //self.promptBottomAlert()
        }`
        `self.view.showBottomAlert(with: "This is Bottom Alert!",type: .warning)`
- [ ] C. Show this alert out of any view :
    `self.topMostController()?.view.showBottomAlert(with: "This is info alert and can be shown from any view like UITableviewCell / UICollectionviewCell / UIView", type: .info)`
    Here above topMostController() is customized in extension page

- To use Network monitor Just use this code `let _ = NetworkMonitor.shared` to you Appdelegate.swift file or The Initialviewcontroller of your project

So, You need to add  view pages like **AlertVIew.xib** and **AlertView.swift** AND the **Extension** folder Under **Utilities**

**Note:-** Copy only the [**"Utility Folder"**](https://gitlab.com/ios-utilities/alertview/-/tree/master/AlertView/Utility)  to your project, thats all.

